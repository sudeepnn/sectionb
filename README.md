## Section B**: Lazy Loading To Avoid Pagination (vanilla js)**

To avoid pagination, Frontend devs came up with the solution of lazy loading, so when a user reaches the end of the page, more results are automatically loaded to have a good user experience.
**Note**: **Please make sure you only use publicly available API**

- Avoid using any libraries.
- Generate any random data, after the user reaches the end of the current results.

# Solution [link](https://sudeepnn.gitlab.io/sectionb/)
